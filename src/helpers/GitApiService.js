/* eslint linebreak-style: ["error", "windows"] */
import gql from 'graphql-tag';
import RepoListItem from '../RepoList/RepoListItem/RepoListItem';

export default {
  /**
   * q: is the query param. It'll be an object containing the query type and value
   * sort: sort response according to a particular trait. e.g follower count, date joined
   * per_page: data returned per page
   */

  searchUsers: gql`
    query SearchForUsers($limit: Int!, $query: String!) {
      search(first: $limit, query: $query, type: USER) {
        userCount
        edges {
          node {
            ...UserInfo
          }
        }
      }
    }
    ${RepoListItem.fragments.user}
  `,

  getHireableUsers(searchResult) {
    const data = searchResult.edges;
    const hireAbleUsers = data.filter(user => user.node.isHireable);
    return {
      ...searchResult,
      edges: hireAbleUsers,
    };
  },

  searchRepos() {},
};
