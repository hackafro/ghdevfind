import React     from 'react';
import PropTypes from 'prop-types';
import Select    from 'react-select';
import 'react-select/dist/react-select.css';

const SideBarForm = ({languages, language, onChange}) => (
  <form>
    { /*<select*/ }
    { /*name="languages"*/ }
    { /*id="languages"*/ }
    { /*onChange={ onChange }*/ }
    { /*value={ language }*/ }
    { /*>*/ }
    { /*{ languages.map((lang, index) => <option key={ index }>{ lang }</option>) }*/ }
    { /*</select>*/ }

    <Select name={ 'language-filter' }
            value={ language }
            onChange={ onChange }
            options={ languages }
    />
  </form>
);

SideBarForm.propTypes = {
  languages: PropTypes.arrayOf(PropTypes.string).isRequired,
  language: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default SideBarForm;
