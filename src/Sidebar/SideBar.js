import React       from 'react';
import PropTypes   from 'prop-types';
import SideBarForm from './SideBarForm/SideBarForm';

import './SideBar.css';

const SideBar = ({onChange, language}) => {
  const languages = [{value: 'javascript', label: 'javascript'}, {value: 'python', label: 'python'}, {
    value: 'java',
    label: 'java'
  }, {value: 'php', label: 'php'}, {value: 'html', label: 'html'}];
  return (
    <div className="gh-sidebar" style={ {'padding': '10px 70px'} }>
      <h3 className="filter-header">Filters</h3>
      <p className="filter-subheader">Language Options</p>
      <SideBarForm
        languages={ languages }
        language={ language }
        onChange={ onChange }
      />
    </div>
  );
};

SideBar.propTypes = {
  onChange: PropTypes.func.isRequired,
  language: PropTypes.string.isRequired,
};

export default SideBar;
