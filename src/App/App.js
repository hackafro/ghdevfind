import React, { Component } from 'react';
import AppBar from './AppBar';

import './App.css';
import AppBody from './AppBody';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filterTerm: '',
    };
    this.baseState = this.state;
    this.filterTermUpdateHandler = this.filterTermUpdateHandler.bind(this);
  }

  filterTermUpdateHandler(e) {
    e.persist();
    // this.setState({ users: this.users });

    this.setState(prevState => ({
      filterTerm: e.target.value,
      users: prevState.users.filter(user =>
        user.login.includes(e.target.value)),
    }));
  }

  render() {
    return (
      <div className="App">
        <div className="" />
        <main className="main-content">
          <AppBar />
          <AppBody />
        </main>
      </div>
    );
  }
}

export default App;
