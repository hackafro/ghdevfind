import React, {Component} from 'react';
import SideBar            from '../Sidebar/SideBar';
import RepoList           from '../RepoList/RepoList';

class AppBody extends Component {
  state = {
    language: 'javascript',
    limit: 30,
  };
  onLanguageChange = this.onLanguageChange.bind(this);

  onLanguageChange(language) {
    this.setState({language: language.value});
  }

  render() {
    return (
      <section className="app-body">
        <SideBar
          language={ this.state.language }
          onChange={ this.onLanguageChange }
        />
        <div className="user-list">
          <RepoList
            language={ this.state.language }
            limit={ this.state.limit }
            ref="list"
          />
        </div>
      </section>
    );
  }
}

export default AppBody;
