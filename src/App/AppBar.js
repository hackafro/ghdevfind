import React       from 'react';
import SearchField from '../SearchBar/SearchField';

const AppHeader = () => (
  <div style={ {textAlign: 'center'} }>
    <h2 className="text-color">GHDevFind</h2>
    <p className="text-color">React tool for searching developers matching a language</p>
    <SearchField/>
  </div>
);

export default AppHeader;
