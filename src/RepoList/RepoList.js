import React from 'react';
import { graphql } from 'react-apollo';
import PropTypes from 'prop-types';

import RepoListItem from './RepoListItem/RepoListItem';
import GhSpinner from '../Spinner/Spinner';
import h from '../helpers/GitApiService';

import './RepoList.css';

const RepoList = ({ loading, error, search }) => {
  if (loading) {
    return (
      <div className="loading">
        <GhSpinner />
      </div>
    );
  }
  if (error) return <h1>Error</h1>;

  const hireableUsers = h.getHireableUsers(search);

  return (
    <React.Fragment>
      {hireableUsers.edges.map(user => (
        <RepoListItem item={user} key={user.node.id.toString()} />
      ))}
    </React.Fragment>
  );
};

RepoList.propTypes = {
  loading: PropTypes.bool.isRequired,
  error: PropTypes.bool,
  search: PropTypes.objectOf(PropTypes.any),
};

export default graphql(h.searchUsers, {
  props: ({ data: { loading, error, search } }) => ({
    loading,
    error,
    search,
  }),
  options: ({ language, limit }) => ({
    variables: { query: `language:${language}`, limit },
  }),
})(RepoList);
