import React from 'react';
import PropTypes from 'prop-types';
import gql from 'graphql-tag';

import './RepoListItem.css';
import RepoListItemBody from './RepoListItemBody';

const RepoListItem = ({ item: { node } }) => {
  const body = {
    login: node.login,
    bio: node.bio,
    isHireable: node.isHireable,
    following: node.following,
    followers: node.followers,
  };

  return (
    <div className="list-item">
      <img
        src={node.avatarUrl}
        alt={node.login}
        className="list-header__image"
      />
      <RepoListItemBody body={body} />
    </div>
  );
};

RepoListItem.fragments = {
  user: gql`
    fragment UserInfo on User {
      id
      login
      bio
      avatarUrl
      isHireable
      followers {
        totalCount
      }
      following {
        totalCount
      }
      name
      location
    }
  `,
};

RepoListItem.propTypes = {
  item: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default RepoListItem;
