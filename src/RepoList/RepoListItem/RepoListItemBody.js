import React from 'react';
import PropTypes from 'prop-types';
import { Briefcase } from 'react-feather';

import Badge from '../../Badge/Badge';
import UserFollowing from './UserFollowing';

const RepoListItemBody = ({ body }) => (
  <div className="list-body">
    <div className="list-header">
      <div className="action-container">
        <h3 className="list-header__text">{body.login}</h3>
        <Badge
          color="lightseagreen"
          text={body.isHireable ? 'Hireable' : 'Unavailable'}
          icon={<Briefcase className="icon" size={13} />}
          fontSize="'11'"
        />
        {/* <button className="follow-button">Follow</button> */}
      </div>
      <UserFollowing
        userDetails={{
          followers: body.followers.totalCount,
          following: body.following.totalCount,
        }}
      />
    </div>
    <div className="list-text">
      <p className="list-body__text">{body.bio}</p>
    </div>
  </div>
);

RepoListItemBody.propTypes = {
  body: PropTypes.shape({
    login: PropTypes.string,
    bio: PropTypes.string,
    isHireable: PropTypes.any,
    following: PropTypes.objectOf(PropTypes.any),
    followers: PropTypes.objectOf(PropTypes.any),
  }).isRequired,
};

export default RepoListItemBody;
