import React from 'react';

const Checkbox = () => (
  <label htmlFor="javascript">
    Javascript
    <input type="checkbox" id="javascript" name="javascript" />
  </label>
);

export default Checkbox;
