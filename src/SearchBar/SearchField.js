import React from 'react';
import './SearchField.css';

const SearchField = (props) => {
  const handleUpdate = (e) => {
    props.updateHandler(e);
  };
  return (
    <input
      placeholder="Enter your search term"
      className="search-bar"
      value={props.text}
      onChange={handleUpdate}
    />
  );
};

export default SearchField;
