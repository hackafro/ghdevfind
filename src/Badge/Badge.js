import React     from 'react';
import PropTypes from 'prop-types';

import './Badge.css';

const Badge = ({
                 color, fontSize, icon, text, title
               }) => {
  return <span className={ `badge ${color}` } style={ {fontSize: `${fontSize}px`} } title={ title }>
    { icon }
    { text }
  </span>;
};

Badge.propTypes = {
  color: PropTypes.string,
  fontSize: PropTypes.string,
  icon: PropTypes.node,
  text: PropTypes.any,
  title: PropTypes.string
};

Badge.defaultProps = {
  color: 'black',
  fontSize: '22',
  icon: '',
  text: '',
  title: ''
};

export default Badge;
